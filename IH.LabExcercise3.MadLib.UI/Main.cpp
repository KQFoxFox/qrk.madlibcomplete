// MadLib
// James Michalski
// Ian
// Quinton



#include <iostream>
#include <conio.h>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

// Easily modified int for sizes of arrays
const int SIZE = 12;

// Empty collection for user input
string arrWords[SIZE]{};

// Array of questions
const string arrQuestion[SIZE] = {
	"Enter an adjective (describing word): ",
	"Enter a sport: ",
	"Enter a city: ",
	"Enter a person: ",
	"Enter an action verb (past tense): ",
	"Enter a vehicle: ",
	"Enter a place: ",
	"Enter a noun(thing, plural): ",
	"Enter an adjective (describing word): ",
	"Enter a food (plural): ",
	"Enter a liquid: ",
	"Enter an adjective (describing word): "
};

// Displaying questions and Getting user input funciton
void GetAnswer(string* pWords)
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << arrQuestion[i];
		getline(cin, pWords[i]);
	}
}

// Saving the madlib function
void saveFile(string path, string lib)
{
	ofstream ofs(path);
	ofs << lib;
	ofs.close();
}


int main()
{
	// Get the answers
	GetAnswer(arrWords);
	cout << "\n";

	// Store the madlib in a variable for saving
	string userLib = "One day my " + arrWords[0] + " and I decided to go to the " + arrWords[1] + " game in "
		+ arrWords[2] + "." + "We really wanted to see " + arrWords[3] + " play. "
		+ "So we " + arrWords[4] + " in the " + arrWords[5] + " and headed down to "
		+ arrWords[6] + " and bought some " + arrWords[7] + "We watched the game and it was "
		+ arrWords[8] + "We ate some " + arrWords[9] + " and drank some " + arrWords[10]
		+ "We had a " + arrWords[11] + " time, and can't wait to go again.";

	// Display the madlib
	cout << "One day my " << arrWords[0] << " friend and I decided to go to the " << arrWords[1] << " game in "
		<< arrWords[2] << "." << "\n"
		<< "We really wanted to see " << arrWords[3] << " play.\n"
		<< "So we " << arrWords[4] << " in the " << arrWords[5] << " and headed down to "
		<< arrWords[6] << " and bought some " << arrWords[7] << ".\n"
		<< "We watched the game and it was " << arrWords[8] << ".\n"
		<< "We ate some " << arrWords[9] << " and drank some " << arrWords[10] << ".\n"
		<< "We had a " << arrWords[11] << " time, and can't wait to go again.\n\n";

	// Where do they wanna save?
	string userPath;

	// Do they wanna save?
	char doIt;

	// Ask
	cout << "save madlib? Y or N" << "\n";

	// Yay or nay
	cin >> doIt;

	// Do what they say i guess
	if (doIt == 'Y' || doIt == 'y')
	{
		cout << "Enter path to save to: ";
		cin >> userPath;

		if (userPath != ".txt")
		{
			userPath += ".txt";
		}

		saveFile(userPath, userLib);
	}

	// Prompt Exit
	cout << "\n" << "Press any key to exit. . ." << "\n";


	(void)_getch();
	return 0;
}